<?php

$i = 0;
$alunos = array(
    0 => array(
        'nome' => 'Adriana',
        'bitbucket' => 'https://bitbucket.org/ragazoni/php'
    ),
    1 => array(
        'nome' => 'Kellyson',
        'bitbucket' => 'https://bitbucket.org/ragazoni/php'
    ),
    2 => array(
        'nome' => 'Vera',
        'bitbucket' => 'https://bitbucket.org/ragazoni/php'
    ),
    3 => array(
        'nome' => 'Beatriz',
        'bitbucket' => 'https://bitbucket.org/ragazoni/php'
    ),
    4 => array(
        'nome' => 'Julia',
        'bitbucket' => 'https://bitbucket.org/ragazoni/php'
    )
);


//tabela 1
echo '<table border="2">
            <thead>
                <th>
                nome
                </th>
                <th>
                Bitbucket
                </th>
            </thead>';

$cor = null;

while ($i < 4) {

    $cor = $cor == 'gray' ? 'white' : 'gray';

    echo "<tr bgcolor='$cor'>                               
                   <th>
                    {$alunos[$i]['nome']}
                   </th>
                   <th>
                    {$alunos[$i]['bitbucket']}
                   </th>
                   </tr>";
    $i++;
}
echo '</table> <br>';




//tabela 2
echo '<table border="2">
        <thead>
            <th>
                 nome
            </th>
            <th>
                 Bitbucket
            </th>
        </thead>';

$cor = null;
$i=0;
while ($i < 4) {
    //$cor = $cor == 'gray' ? 'white' : 'gray';

     if($cor == 'gray'){
         $cor = 'white';
     }else{
         $cor = 'gray';
     }

    echo "<tr bgcolor='$cor'>                               
            <th>
             {$alunos[$i]['nome']}
            </th>
            <th>
             {$alunos[$i]['bitbucket']}
            </th>
            </tr>";
    $i++;
}
echo '</table> <br><br>';


//operadores === e !==
$nome ='Adriana Diniz Ragazoni Bezerra';
$posicao = strpos($nome, 'Adriana');

if($posicao !== false){
    echo "encontrado! $posicao";
}

