<?php 

class Usuario{
    private  $id;
    private  $nome;
    private  $email;
    private  $senha;

    public function __construct(){
        $this->objDb = new mysqli('localhost', 'root', '', 'aula_php', 3306);

       
    }    

   public function setId(int $id){
        $this->id = $id;
    }

     public function setNome(string $nome){
        $this->nome = $nome;
    }

     public function setEmail(string $email){
        $this->email = $email;
    }
     public function setSenha(string $senha){
         $this->senha = password_hash($senha, PASSWORD_DEFAULT);
        
    }

    public function getId(int $id): int {
        return $this->id = $id;
    }

    public function getNome(string $nome): string{
        return $this->nome = $nome;
    }

    public function getEmail(string $email): string{
        return $this->email = $email;
    }

    public function getSenha(string $senha): string{
        return $this->senha = $senha;
    }

    public function getUser (int $id){
        return array($this->id, $this->nome, $this->email);
    }

    public function saveUsuario()
    {

        //preparar consulta 
        $objStmt = $this->objDb->prepare('REPLACE INTO tb_usuario (id, nome,email,senha) values (?,?,?,?)');
        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);
        
        if ($objStmt->execute() ) {
            return true;
        } else {
            return false;
        }
    }

    public function listUsuario(){

        $objResult = $this->objDb->prepare('SELECT nome, email FROM tb_usuario WHERE id = ?' );
        $objResult->bind_param('i', $this->id);
        $objResult->execute();       
        $resultado = $objResult->get_result();
        $resultado->fetch_assoc();
        echo '<pre>';
        var_dump($resultado);
     
       }
       
           
       
    

    public function apagaUsuario(int $id){

    }

    public function __destruct()
    {
        $this->objDb;
    }

    
}









